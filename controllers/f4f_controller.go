/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"

	webappv1 "f4f-operator/api/v1"
	"math/rand"
	"strings"
	"time"

	"github.com/go-logr/logr"
	kbatch "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// F4FReconciler reconciles a F4F object
type F4FReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

// +kubebuilder:rbac:groups=webapp.my.domain,resources=f4fs,verbs=*
// +kubebuilder:rbac:groups=webapp.my.domain,resources=f4fs/status,verbs=*
// +kubebuilder:rbac:groups="",resources=nodes;pods;jobs,verbs=*
// +kubebuilder:rbac:groups="webappv1",resources=nodes;pods;jobs,verbs=*
// +kubebuilder:rbac:groups="batch",resources=nodes;pods;jobs,verbs=*
// +kubebuilder:rbac:groups=apps,resources=daemonsets,verbs=get;list;watch;create;update;patch;delete

func (r *F4FReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {

	ctx := context.Background()
	log := r.Log.WithValues("f4f", req.NamespacedName)

	//Fetch the F4F instance
	instance := &webappv1.F4F{}

	// Check if F4F instance already exists

	if err := r.Get(ctx, req.NamespacedName, instance); err != nil {

		if errors.IsNotFound(err) {
			log.Info("F4F resource not found. Ignoring since object must be deleted")
			return ctrl.Result{}, nil
		}
		log.Error(err, "Unable to fetch F4F-Operator")
		return ctrl.Result{}, err
	}
	log.V(1).Info("Phase", "Mapping Phase", instance.Spec.Phase)

	log.V(1).Info("Workflow", "Workflow Size", len(instance.Spec.Work))

	currentPhase := instance.Spec.Phase

	if int(currentPhase) <= len(instance.Spec.Work) {

		// Get nodes with specific label
		nodeList := &corev1.NodeList{}
		err := r.Client.List(context.TODO(), nodeList, client.MatchingLabels{"test_label": "f4f-exp"})
		if err != nil {
			log.Error(err, "Failed to list nodes.")
			return ctrl.Result{}, err
		}

		log.V(1).Info("Phase:", "Name:", instance.Spec.Work[currentPhase-1].Name, "Image:", instance.Spec.Work[currentPhase-1].Image, "WorkLabel:", instance.Spec.Work[currentPhase-1].WorkLabel)
		nodeNames := getNodeNames(nodeList.Items)

		if instance.Spec.ExperimentId == "" {
			instance.Spec.ExperimentId = instance.Name + "-" + instance.Spec.ComparisonType
			if err := r.Client.Update(context.TODO(), instance); err != nil {
				log.Error(err, "failed to update Experiment Id")
				return ctrl.Result{}, err
			}
		}

		if instance.Spec.Work[currentPhase-1].WorkLabel == "" {
			// For every node, crate a job
			for _, nodeName := range nodeNames {

				found := &kbatch.Job{}
				err = r.Get(ctx, types.NamespacedName{Name: instance.Spec.Work[currentPhase-1].Name + "-" + nodeName + "-" + strings.ToLower(instance.Spec.ExperimentId), Namespace: "default"}, found)

				if err != nil && errors.IsNotFound(err) {
					job := r.constructJob(instance, nodeName, instance.Spec.Work[currentPhase-1].Name, instance.Spec.Work[currentPhase-1].Image, instance.Spec.ComparisonType, instance.Spec.ExperimentId)
					if err := r.Create(ctx, job); err != nil {
						log.Error(err, "unable to create Job", "Job", job)
						return ctrl.Result{}, err
					}
					log.Info("Creating a new job", "Job.Name", job.Name)
					return ctrl.Result{Requeue: true}, nil
				} else if err != nil {
					log.Error(err, "Failed to get Job")
					return ctrl.Result{}, err
				}

			}
		} else {
			found := &kbatch.Job{}
			err = r.Get(ctx, types.NamespacedName{Name: instance.Spec.Work[currentPhase-1].Name, Namespace: "default"}, found)

			if err != nil && errors.IsNotFound(err) {
				job := r.constructReduceJob(instance, instance.Spec.Work[currentPhase-1].Name, instance.Spec.Work[currentPhase-1].Image, instance.Spec.ExperimentId)
				if err := r.Create(ctx, job); err != nil {
					log.Error(err, "unable to create Job", "Job", job)
					return ctrl.Result{}, err
				}
				log.Info("Creating a new reduce job", "Job.Name", job.Name)
				return ctrl.Result{Requeue: true}, nil
			} else if err != nil {
				log.Error(err, "Failed to get Job")
				return ctrl.Result{}, err
			}

		}

		jobs := &kbatch.JobList{}

		err = r.Client.List(context.TODO(), jobs, client.MatchingLabels{"my_label": "f4f-operator"})

		if err != nil {
			log.Error(err, "Failed to list jobs.")
			return ctrl.Result{}, err
		}

		var successfulJobs []*kbatch.Job

		totalJobs := len(jobs.Items)
		log.V(1).Info("Number of Jobs", "number of jobs:", totalJobs)
		for i, job := range jobs.Items {
			if job.Status.Succeeded == 1 {
				completionTime := job.Status.CompletionTime.Time
				startTime := job.Status.StartTime.Time
				diff := completionTime.Sub(startTime)
				successfulJobs = append(successfulJobs, &jobs.Items[i])
				log.V(1).Info("Finished Job:", "job name", job.Name, "cpu time", diff)
				// log.V(1).Info("Number of finished jobs", "number_of_finished", len(successfulJobs))
			}
		}
		if len(successfulJobs) == totalJobs {
			log.V(1).Info("Done with the mapping phase of the experimenct. Reduce is next!")
			instance.Spec.Phase += 1
			if err := r.Client.Update(context.TODO(), instance); err != nil {
				log.Error(err, "failed to update MappingPhase")
				return ctrl.Result{}, err
			}
			// for _, job := range successfulJobs {
			// 	if err := r.Delete(ctx, job, client.PropagationPolicy(metav1.DeletePropagationBackground)); (err) != nil {
			// 		log.Error(err, "unable to delete old successful job", "job", job)
			// 	} else {
			// 		log.V(0).Info("deleted old successful job", "job", job)
			// 	}
			// }
		}
	}
	return ctrl.Result{}, nil
}

func (r *F4FReconciler) constructJob(f *webappv1.F4F, nodeName string, jobName string, imageName string, comparisonType string, experimentId string) *kbatch.Job {

	job := &kbatch.Job{
		ObjectMeta: metav1.ObjectMeta{
			Name:      jobName + "-" + nodeName + "-" + strings.ToLower(experimentId),
			Namespace: "default",
		},
		Spec: kbatch.JobSpec{
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "f4f" + "-" + jobName + "-" + nodeName + "-" + strings.ToLower(experimentId),
					Namespace: "default",
					Labels:    map[string]string{"my_label": "f4f-operator", "experiment_id": experimentId},
				},
				Spec: corev1.PodSpec{
					NodeSelector: map[string]string{"f4f-app": "f4f-" + nodeName},
					Containers: []corev1.Container{
						{
							Name:  jobName,
							Image: imageName,
							Env: []corev1.EnvVar{
								{
									Name:  "comparisonType",
									Value: comparisonType,
								},
								{
									Name:  "experimentId",
									Value: "experiment-" + experimentId,
								},
							},
							VolumeMounts: []corev1.VolumeMount{
								{
									Name:      "my-input-volume",
									MountPath: "/test/data-in/",
								},
								{
									Name:      "my-output-volume",
									MountPath: "/test/data-out/",
								},
								{
									Name:      "shared-queries-storage",
									MountPath: "/shared/",
								},
							},
						},
					},
					RestartPolicy: corev1.RestartPolicyOnFailure,
					Volumes: []corev1.Volume{
						{
							Name: "my-input-volume",
							VolumeSource: corev1.VolumeSource{
								HostPath: &corev1.HostPathVolumeSource{
									Path: "/test/data-in/",
								},
							},
						},
						{
							Name: "my-output-volume",
							VolumeSource: corev1.VolumeSource{
								HostPath: &corev1.HostPathVolumeSource{
									Path: "/test/data-out/",
								},
							},
						},
						{
							Name: "shared-queries-storage",
							VolumeSource: corev1.VolumeSource{
								PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
									ClaimName: "fair4fusion-demo-storage",
								},
							},
						},
					},
				},
			},
		},
	}
	ctrl.SetControllerReference(f, job, r.Scheme)

	return job

}

func (r *F4FReconciler) constructReduceJob(f *webappv1.F4F, jobName string, imageName string, experimentId string) *kbatch.Job {

	job := &kbatch.Job{
		ObjectMeta: metav1.ObjectMeta{
			Name:      jobName + strings.ToLower(experimentId),
			Namespace: "default",
		},
		Spec: kbatch.JobSpec{
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Name:      jobName + "-" + strings.ToLower(experimentId),
					Namespace: "default",
					Labels:    map[string]string{"my_label": "f4f-operator"},
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "f4f-" + jobName + "-" + strings.ToLower(experimentId),
							Image: imageName,
							Env: []corev1.EnvVar{
								{
									Name:  "experimentId",
									Value: "experiment-" + experimentId,
								},
							},
							// Command: []string{"sh", "-c"},
							// Args:    []string{"echo hello from mapping phase world && sleep 30"},

							VolumeMounts: []corev1.VolumeMount{
								{
									Name:      "my-input-volume",
									MountPath: "/test/data-in/",
								},
								{
									Name:      "my-output-volume",
									MountPath: "/test/data-out/",
								},
								{
									Name:      "shared-queries-storage",
									MountPath: "/shared/",
								},
							},
						},
					},
					RestartPolicy: corev1.RestartPolicyOnFailure,
					Volumes: []corev1.Volume{
						{
							Name: "my-input-volume",
							VolumeSource: corev1.VolumeSource{
								HostPath: &corev1.HostPathVolumeSource{
									Path: "/test/data-in/",
								},
							},
						},
						{
							Name: "my-output-volume",
							VolumeSource: corev1.VolumeSource{
								HostPath: &corev1.HostPathVolumeSource{
									Path: "/test/data-out/",
								},
							},
						},
						{
							Name: "shared-queries-storage",
							VolumeSource: corev1.VolumeSource{
								PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
									ClaimName: "fair4fusion-demo-storage",
								},
							},
						},
					},
				},
			},
		},
	}
	ctrl.SetControllerReference(f, job, r.Scheme)

	return job

}

// getPodNames returns the pod names of the array of pods passed in
func getNodeNames(nodes []corev1.Node) []string {
	var nodeNames []string
	for _, node := range nodes {
		nodeNames = append(nodeNames, node.Name)
	}
	return nodeNames
}

func getFinishedStatus(j *kbatch.Job) (bool, kbatch.JobConditionType) {
	for _, c := range j.Status.Conditions {
		if c.Type == kbatch.JobComplete || c.Type == kbatch.JobFailed {
			return true, c.Type
		}
	}
	return false, ""
}

// IsJobFinished returns whether or not a job has completed successfully or failed.
func IsJobFinished(j *kbatch.Job) bool {
	isFinished, _ := getFinishedStatus(j)
	return isFinished
}

func RandomString(n int) string {
	rand.Seed(time.Now().Unix())
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	s := make([]rune, n)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}

func (r *F4FReconciler) SetupWithManager(mgr ctrl.Manager) error {

	return ctrl.NewControllerManagedBy(mgr).
		For(&webappv1.F4F{}).Owns(&kbatch.Job{}).
		Complete(r)
}
