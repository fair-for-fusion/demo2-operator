/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// F4FSpec defines the desired state of F4F

type Workflow struct {
	Image     string `json:"image"`
	Name      string `json:"name"`
	WorkLabel string `json:"workLabel,omitempty"`
}

type F4FSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	Work           []Workflow `json:"work"`
	ComparisonType string     `json:"comparisonType,omitempty"`
	// Label is the value of the 'daemon=' label to set on a node that should run the daemon

	// Mapping phase is set to true if we currently are in mapping phase.When we proceed to reduce phase, it becomes false
	Phase        int32  `json:"Phase"`
	ExperimentId string `json:"experimentId,omitempty"`
}

// F4FStatus defines the observed state of F4F
type F4FStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	// Count is the number of nodes the image is deployed to
	Count int32 `json:"count"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// F4F is the Schema for the f4fs API
type F4F struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   F4FSpec   `json:"spec,omitempty"`
	Status F4FStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// F4FList contains a list of F4F
type F4FList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []F4F `json:"items"`
}

func init() {
	SchemeBuilder.Register(&F4F{}, &F4FList{})
}
